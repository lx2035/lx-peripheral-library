/*
 * fly_ibus.h
 *
 *  Created on: Feb 13, 2022
 *      Author: LX
 */

#ifndef FLY_IBUS_H_
#define FLY_IBUS_H_

#include "main.h"
#include "usart.h"

/* User configuration */
#define IBUS_UART				(&huart1)
#define IBUS_UART_INSTANCE		(USART1)
#define IBUS_USER_CHANNELS		10			// Use 6 channels
/* User configuration */

#define IBUS_LENGTH				0x20	// 32 bytes
#define IBUS_COMMAND40			0x40	// Command to set servo or motor speed is always 0x40
#define IBUS_MAX_CHANNLES		14

void IBUS_INIT();
void IBUS_READ_CHANNEL(uint8_t user_channels);

#endif /* FLY_IBUS_H_ */
