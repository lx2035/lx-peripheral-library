## 使用富斯遥控器：

>　使用逻辑分析仪读取代码


![在这里插入图片描述](https://img-blog.csdnimg.cn/d5799b32a96c4be4b7184faaeb16f783.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5qGD5oiQ6LmKMi4w,size_20,color_FFFFFF,t_70,g_se,x_16)

每帧是32个字节，解码代码如下所示，采用串口中断或者DMA接收32个长度的字节

```c
void IBUS_READ_CHANNEL(uint8_t user_channels)
{
	uint16_t channel_buffer[IBUS_MAX_CHANNLES] = {0};

	if(rx_buffer[0] == IBUS_LENGTH && rx_buffer[1] == IBUS_COMMAND40)
	{
		checksum_cal = 0xffff - rx_buffer[0] - rx_buffer[1];

		for(int i = 0; i < IBUS_MAX_CHANNLES; i++)
		{
			channel_buffer[i] = (uint16_t)(rx_buffer[i * 2 + 3] << 8 | rx_buffer[i * 2 + 2]);
			checksum_cal = checksum_cal - rx_buffer[i * 2 + 3] - rx_buffer[i * 2 + 2];
		}

		checksum_ibus = rx_buffer[31] << 8 | rx_buffer[30];

		if(checksum_cal == checksum_ibus)
		{
			for(int j = 0; j < user_channels; j++)
			{
				channel[j] = channel_buffer[j];
			}
		}
	}

	HAL_UART_Receive_IT(IBUS_UART, rx_buffer, 32);
}
```
用到的宏
```c
#define IBUS_UART				(&huart1)
#define IBUS_UART_INSTANCE		(USART1)
#define IBUS_USER_CHANNELS		10		
/* User configuration */

#define IBUS_LENGTH				0x20	
#define IBUS_COMMAND40			0x40	
#define IBUS_MAX_CHANNLES		14
```
需要注意，系统时钟一定要开足，因为系统时钟不行一直乱码，跑不起来，可能是配置问题也不一定
![在这里插入图片描述](https://img-blog.csdnimg.cn/53de999af8ce4794b2586be1fd64ead5.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5qGD5oiQ6LmKMi4w,size_20,color_FFFFFF,t_70,g_se,x_16)
效果如下所示
![在这里插入图片描述](https://img-blog.csdnimg.cn/231f98ef808e48e8aaac11ab1701d746.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5qGD5oiQ6LmKMi4w,size_20,color_FFFFFF,t_70,g_se,x_16)